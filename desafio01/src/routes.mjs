import { buildRoutePath } from "./utils/build-route-path.mjs"
import { randomUUID } from 'node:crypto'
import { Database } from "./database/index.mjs"
import { LeitorCsv } from "./utils/leitorCsv.mjs"

const database = new Database()
const csv = new LeitorCsv()

export const routes = [
  {
    method: 'GET',
    path: buildRoutePath('/tasks'),
    handler: (request, response) => {

      const tasks = database.select('tasks')


      return response.end(JSON.stringify(tasks))
    }
  },
  {
    method: 'GET',
    path: buildRoutePath('/tasks/:id'),
    handler: (request, response) => {
      const { id } = request.params

      const task = database.select('tasks', id)

      if(task.length === 0){
        const message = {
          message: "Task not found"
        }

        return response
          .writeHead(404)
          .end(JSON.stringify(message))
      }

      return response.end(JSON.stringify(task))
    }
  },
  {
    method: 'POST',
    path: buildRoutePath('/tasks'),
    handler: (request, response) => {
      const { title, description } = request.body

      if(!title || !description) {
        return response
          .writeHead(400)
          .end(JSON.stringify({
            "message": "title and description are required"
          }))
      }

      const task = {
        id: randomUUID(),
        title,
        description,
        completed_at: null,
        created_at: new Date(),
        updated_at: null
      }
      
      database.insert('tasks',task)

      return response
        .writeHead(201)
        .end(JSON.stringify(task))
    }
  },
  {
    method: 'POST',
    path: buildRoutePath('/tasks/csv'),
    handler: async (request, response) => {

      const datas = csv.getData()
      for await (const data of datas) {
        const task = {
          id: randomUUID(),
          title: data.title,
          description: data.description,
          completed_at: null,
          created_at: new Date(),
          updated_at: null
        }
        database.insert('tasks', task)
      }

      return response
        .writeHead(201)
        .end()
    }
  },
  {
    method: 'PUT',
    path: buildRoutePath('/tasks/:id'),
    handler: (request, response) => {
      const { title, description } = request.body
      const { id } = request.params

      try {
        database.update('tasks', id, {
          title,
          description,
          updated_at: new Date()
        })
      } catch(e) {
        return response
          .writeHead(404)
          .end(JSON.stringify({
            "code": e.code,
            "message": e.message
          }))
      }

      return response.writeHead(204).end()
    }
  },
  {
    method: 'PUT',
    path: buildRoutePath('/tasks/:id/complete'),
    handler: (request, response) => {
      const { id } = request.params

      try {
        database.update('tasks', id, {}, true)
      } catch(e) {
        return response
          .writeHead(404)
          .end(JSON.stringify({
            "code": e.code,
            "message": e.message
          }))
      }

      return response.writeHead(204).end()
    }
  },
  {
    method: 'DELETE',
    path: buildRoutePath('/tasks/:id'),
    handler: (request, response) => {
      const { id } = request.params
      
      try {
        database.delete('tasks', id)
      } catch(e) {
        return response
          .writeHead(404)
          .end(JSON.stringify({
            "code": e.code,
            "message": e.message
          }))
      }

      return response.writeHead(204).end()
    }
  }
]
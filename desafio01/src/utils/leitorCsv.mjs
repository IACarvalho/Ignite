import fs from 'node:fs/promises'

const filePath = new URL('../../tasks.csv', import.meta.url)

export class LeitorCsv {
  #data = []

  constructor() {
    fs.readFile(filePath, 'utf8')
      .then(async (data) => {
        const parcialData = data.split('\n')
        for await(const line of parcialData){
          if(line === 'title,description')
            continue
          const title = line.split(',')[0]
          const description = line.split(',')[1]
          this.#data.push(
            { "title" : title,
            "description": description
          })
        }
      })
  }

  getData() {
    return this.#data
  }
}
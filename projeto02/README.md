# Porjeto 02

## Knex

Comando para criar uma migration, lembrando que esta usando o script e uma gambiarra
```bash
npm run kenx -- migrate:make nome-da-migration
```

Comando para executar as ações da migration
```bash
npm run knex -- migrate:latest
```

Caso não tenha sido dividida com o restante da equipe e queira alterar uma migration use o comando
```bash
npm run knex -- migrate:rollback
```
**Mas caso tenha sido compatrtilhada crie uma nova, uma migration executada nunca mais sera executada novamente**

## Requisitos e Regras de negócio

### RF
- [x] O usuario deve poder criar uma nova trancacao;
- [x] O usuario deve poder obeter um resumo de sua conta;
- [x] O usuario deve poder listar todas as transacoes que ja ocorreram;
- [x] O usuario deve poder visualizaruma transacao unica;

### RN
- [ ] A transacao pode ser do tipo credito (adicionarar o valor) ou debito (subtraira o valor);
- [ ] Deve ser possivel identificar o usuario entre as requisicoes;

### RNF
*Requisitos mais tecnicos onde sera descrito como satisfazer as regras de negocio*

## Types com knext
Apesar do knext nao ser tao completo quanto um ORM ele possibilita a criacao de typos para as tables facilitando manutencoes futuras

## Cookies
São formas de manter contexto entre requisições
Um cookie quando criado é acessível por todo o domain e são enviados automaticamente por toda requisição